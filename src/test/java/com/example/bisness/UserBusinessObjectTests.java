package com.example.bisness;

import com.example.Data.Notification;
import com.example.Data.User;
import com.example.business.UserBusinessObject;
import com.example.userDAO.UserDAOStub;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Summoner on 19.10.2017.
 */
public class UserBusinessObjectTests {
    private UserBusinessObject userBusinessObject;

    @Before
    public void before() {
        UserDAOStub userDAOStub = new UserDAOStub();
        userBusinessObject = new UserBusinessObject(userDAOStub);
    }

    @Test
    public void dsUserMassCreateTest() throws SQLException {
        List<User> users = new ArrayList<User>();
        users.add(new User().setUser_id(1).setLogin("pchupin").setPassword("14855887").setFirst_name("pavel").setLast_name("chupin").setGender(1).setBirth_date("1984-06-11"));
        users.add(new User().setUser_id(2).setLogin("pchupin1").setPassword("148558871").setFirst_name("pavel").setLast_name("chupin").setGender(0).setBirth_date("1984-06-11"));
        List<Notification> notifications = userBusinessObject.dsUserMassCreate(users);
        assertEquals(notifications.size(), 2);
    }

    @Test
    public void dsUserMassDeleteTest() throws Exception {
        List<Integer> usersID = new ArrayList<Integer>();
        usersID.add(1);
        usersID.add(2);
        List<Notification> notifications = userBusinessObject.dsUserMassDelete(usersID);
        assertEquals(notifications.size(), 2);
    }


    @Test
    public void dsUserFindListByParamTest() throws Exception {
        List<User> users = userBusinessObject.dsUserFindListByParam("", "", "");
        assertEquals(users.size(), 2);
    }


}
