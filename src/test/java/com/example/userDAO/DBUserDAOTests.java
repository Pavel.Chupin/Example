package com.example.userDAO;

import com.example.Data.User;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Summoner on 18.10.2017.
 */
public class DBUserDAOTests {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String JDBC_URL = "jdbc:h2:mem:test";
    private static final String USER_LOGIN = "sa";
    private static final String USER_PASSWORD = "";
    protected DBUserDAO dbUserDAO;
    protected IDatabaseTester tester;
    protected IDataSet beforeData;

    @Before
    public void before() throws Exception {
        dbUserDAO = new DBUserDAO(JDBC_URL, USER_LOGIN, USER_PASSWORD);
        //Инициализируем DBUnit
        tester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER_LOGIN, USER_PASSWORD);
        //Заполним базу
        insertDataSettoDB();
    }

    @Test
    public void deleteUserSizeTest() throws Exception {
        List<User> beforeusers = dbUserDAO.selectUser("", "", "", 0);
        List<Integer> usersID = new ArrayList<Integer>();
        usersID.add(new Integer(1));
        usersID.add(new Integer(2));
        //Запускаем удаление
        dbUserDAO.deleteUser(usersID);
        List<User> afterusers = dbUserDAO.selectUser("", "", "", 0);
        assertEquals(beforeusers.size(), afterusers.size() + 2);
    }

    @Test
    public void deleteUserHashTest() throws Exception {

        List<User> beforeusers = dbUserDAO.selectUser("", "", "", 0);
        List<Integer> usersID = new ArrayList<Integer>();
        usersID.add(beforeusers.get(1).getUser_id());
        beforeusers.remove(1);
        //beforeusers.clear();//removeAll(beforeusers);
        //Запускаем удаление
        dbUserDAO.deleteUser(usersID);
        List<User> afterusers = dbUserDAO.selectUser("", "", "", 0);
        assertEquals(new HashSet<Object>(beforeusers), new HashSet<Object>(afterusers));
    }

    @Test
    public void selectUserSizeTest() throws Exception {
        List<User> users = dbUserDAO.selectUser("", "", "", 0);
        assertTrue(users.size() == 2);
    }

    @Test
    public void insertUserSizeTest() throws SQLException {
        List<User> beforeusers = dbUserDAO.selectUser("", "", "", 0);
        List<User> users = new ArrayList<User>();
        users.add(new User().setUser_id(3).setLogin("pchupin").setPassword("14855887").setFirst_name("pavel").setLast_name("chupin").setGender(1).setBirth_date("1984-06-11").setDescription("test1"));
        users.add(new User().setUser_id(4).setLogin("pchupin1").setPassword("148558871").setFirst_name("pavel").setLast_name("chupin").setGender(0).setBirth_date("1984-06-11").setDescription("test1"));
        //Запускаем добавление
        dbUserDAO.insertUser(users);
        //Получаем измененый список
        List<User> afterusers = dbUserDAO.selectUser("", "", "", 0);
        //Запускаем проверки
        assertEquals(beforeusers.size() + 2, afterusers.size());
    }

    @Test
    public void insertUserHashTest() throws SQLException {
        List<User> beforeusers = dbUserDAO.selectUser("", "", "", 0);
        List<User> users = new ArrayList<User>();
        users.add(new User().setUser_id(3).setLogin("pchupin1").setPassword("148558871").setFirst_name("pavel").setLast_name("chupin").setGender(0).setBirth_date("1984-06-11").setDescription("test1"));
        beforeusers.add(users.get(0));
        //Запускаем добавление
        dbUserDAO.insertUser(users);
        //Получаем измененый список
        List<User> afterusers = dbUserDAO.selectUser("", "", "", 0);
        //Запускаем проверки
        assertEquals(new HashSet<Object>(beforeusers), new HashSet<Object>(afterusers));
    }

    @After
    public void afterDeleteUserTest() throws SQLException {
        dbUserDAO.connectionClose();
    }

    private void insertDataSettoDB() throws Exception {
        //Получим дата сет для заполнения таблици
        beforeData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("user-data.xml"));
        //Заполним базу тестовыми данными
        tester.setDataSet(beforeData);
        tester.onSetup();
    }
}
