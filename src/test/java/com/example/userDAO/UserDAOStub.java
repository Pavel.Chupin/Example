package com.example.userDAO;

import com.example.Data.User;
import org.apache.cayenne.CayenneRuntimeException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Summoner on 19.10.2017.
 */
public class UserDAOStub implements UserDAO {

    public void insertUser(List<User> users) throws CayenneRuntimeException {
        //return;
    }

    public List<User> selectUser(String login, String firstName, String lastName, Integer userID) throws CayenneRuntimeException {
        List<User> users = new ArrayList<User>();
        users.add(new User().setUser_id(1).setLogin("pchupin").setPassword("14855887").setFirst_name("pavel").setLast_name("chupin").setGender(1).setBirth_date("1984-06-11").setDescription("test1"));
        users.add(new User().setUser_id(2).setLogin("pchupin1").setPassword("148558871").setFirst_name("pavel").setLast_name("chupin").setGender(0).setBirth_date("1984-06-11").setDescription("test1"));
        return users;
    }

    public void deleteUser(List<Integer> usersID) throws CayenneRuntimeException {
        //return;
    }
}
