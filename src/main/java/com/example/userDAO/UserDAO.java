package com.example.userDAO;

import com.example.Data.User;
import org.apache.cayenne.CayenneRuntimeException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Summoner on 19.10.2017.
 */
public interface UserDAO {

    public void insertUser(List<User> users) throws CayenneRuntimeException;

    public List<User> selectUser(String login, String firstName, String lastName, Integer userID) throws CayenneRuntimeException;

    public void deleteUser(List<Integer> usersID) throws CayenneRuntimeException;
}
