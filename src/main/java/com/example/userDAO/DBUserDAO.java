package com.example.userDAO;

import com.example.Data.User;
import org.apache.cayenne.CayenneRuntimeException;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.NamedQuery;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import ru.proghouse.cayenne.persistent.Tuser;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Summoner on 17.10.2017.
 */
public class DBUserDAO implements UserDAO {
    private Connection connection = null;
    private VelocityEngine velocity;
    private ObjectContext context;
    private ServerRuntime cayenneRuntime;

    private Connection getConnection() {
        return connection;
    }

    private ObjectContext getContext() {
        return context;
    }

    private VelocityEngine getVelocity() {
        if (velocity != null) {
            return velocity;
        }
        Properties p = new Properties();
        p.put("input.encoding", "UTF-8");
        p.put("output.encoding", "UTF-8");
        p.put("resource.loader", "file");
        p.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        p.put("file.resource.loader.path", "src/main/resources");

        this.velocity = new VelocityEngine();
        this.velocity.init(p); // инициализация Velocity
        return velocity;
    }

    public DBUserDAO(String url, String user, String password) {
        //Устанавливаем соединение с базой
        try {
            Class.forName("org.h2.Driver");
            try {
                this.connection = DriverManager.getConnection(url, user, password);
                //Отключаем автокоммит транзакций
                this.connection.setAutoCommit(false);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        this.cayenneRuntime = new ServerRuntime("cayenne-project.xml");
        //this.cayenneRuntime.getDataDomain().setUsingExternalTransactions(true);
        this.context = cayenneRuntime.getContext();

        NamedQuery createTableUser = new NamedQuery("createTableUser");
        context.performQuery(createTableUser);
    }

    public void insertUser(List<User> users) throws CayenneRuntimeException {
        if (users == null || users.isEmpty()) {
            return;
        }

        for (User user : users) {
            //VelocityEngine velocity = getVelocity();
            VelocityContext vc = new VelocityContext(); // создание контекста Velocity
            vc.put("user_id", user.getUser_id()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("login", user.getLogin()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("password", user.getPassword()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("first_name", user.getFirst_name()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("last_name", user.getLast_name()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("gender", user.getGender()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("birth_date", user.getBirth_date()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            vc.put("description", user.getDescription()); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
            Template template = getVelocity().getTemplate("insert.vm"); // загрузка шаблона с именем template.vm
            StringWriter sw = new StringWriter();
            template.merge(vc, sw);

            SQLTemplate insert = new SQLTemplate(Tuser.class, sw.toString());
            try {
                getContext().performQuery(insert);
                getContext().commitChanges();
            } catch (CayenneRuntimeException e) {
                getContext().rollbackChanges();
                throw e;
            }
        }

    }

    public List<User> selectUser(String login, String firstName, String lastName, Integer userID) throws CayenneRuntimeException {
        List<User> users = new ArrayList<User>();
        String suserID = "";
        if (login == null || login.equals("")) {
            login = "%";
        }
        if (firstName == null || firstName.equals("")) {
            firstName = "%";
        }
        if (lastName == null || lastName.equals("")) {
            lastName = "%";
        }
        if (userID == 0) {
            suserID = "%";
        } else {
            suserID = userID.toString();
        }

        //VelocityEngine velocity = getVelocity();
        VelocityContext vc = new VelocityContext(); // создание контекста Velocity
        vc.put("login", login); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
        vc.put("firstName", firstName); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
        vc.put("lastName", lastName); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
        vc.put("suserID", suserID); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
        Template template = getVelocity().getTemplate("select.vm"); // загрузка шаблона с именем template.vm
        StringWriter sw = new StringWriter();
        template.merge(vc, sw);

        SQLTemplate select = new SQLTemplate(Tuser.class, sw.toString());
        try {
            List<Tuser> tusers = getContext().performQuery(select);
            for (Tuser tuser : tusers) {
                users.add(new User().setUser_id(tuser.getUser_id()).setLogin(tuser.getLogin())
                        .setPassword(tuser.getPassword()).setFirst_name(tuser.getFirstName())
                        .setLast_name(tuser.getLastName()).setGender(tuser.getGender()).setBirth_date(tuser.getBirthDate())
                        .setDescription(tuser.getDescrirtion()));
            }
        } catch (CayenneRuntimeException e) {
            throw e;
        }
        return users;
    }

    public void deleteUser(List<Integer> usersID) throws CayenneRuntimeException {
        if (usersID == null || usersID.isEmpty()) {
            return;
        }

        /*Statement statement = null;
        try {
            statement = getConnection().createStatement();
*/
        String s = usersID.get(0).toString();
        for (int i = 1; i < usersID.size(); i++) {
            s = s + " ," + usersID.get(i).toString();
        }

        //VelocityEngine velocity = getVelocity();
        VelocityContext vc = new VelocityContext(); // создание контекста Velocity
        vc.put("usersIDDelete", s); // атрибут "name" связывается с именем переменной $foo в шаблоне и помещается в контекст
        Template template = getVelocity().getTemplate("delete.vm"); // загрузка шаблона с именем template.vm
        StringWriter sw = new StringWriter();
        template.merge(vc, sw);

        SQLTemplate delete = new SQLTemplate(Tuser.class, sw.toString());
        try {
            getContext().performQuery(delete);
            getContext().commitChanges();
        } catch (CayenneRuntimeException e) {
            getContext().rollbackChanges();
            throw e;
        }

    }


    public void connectionClose() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

}
