package com.example.Data;

import java.util.Date;

/**
 * Created by Summoner on 17.10.2017.
 */
public class User {
    private int user_id;
    //public static final int MALE = 1;
    //public static final int FIMALE = 2;

    private String login;
    private String password;
    private String first_name;
    private String last_name;
    private int gender;
    private String birth_date;
    private String description;

    public int getUser_id() {
        return user_id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getGender() {
        return gender;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getDescription() {
        return description;
    }

    public User setLogin(String login) {
        this.login = login;
        return this;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public User setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public User setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (user_id != user.user_id) return false;
        if (gender != user.gender) return false;
        if (!login.equals(user.login)) return false;
        if (!password.equals(user.password)) return false;
        if (!first_name.equals(user.first_name)) return false;
        if (!last_name.equals(user.last_name)) return false;
        return birth_date.equals(user.birth_date);
    }

    @Override
    public int hashCode() {
        int result = user_id;
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + first_name.hashCode();
        result = 31 * result + last_name.hashCode();
        result = 31 * result + gender;
        result = 31 * result + birth_date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Tuser{" +
                "user_id=" + user_id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", gender=" + gender +
                ", birth_date='" + birth_date + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public User setGender(int gender) {
            this.gender = gender;
        return this;
    }

    public User setBirth_date(String birth_date) {
        this.birth_date = birth_date;
        return this;
    }

    public User setDescription(String description) {
        this.description = description;
        return this;
    }


    public User setUser_id(int user_id) {
        this.user_id = user_id;
        return this;
    }
}
