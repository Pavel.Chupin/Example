package com.example.Data;

/**
 * Created by Summoner on 17.10.2017.
 */
public class Notification {
    private int userID;
    private int ntfID;
    private String ntfMessage;

    public int getUserID() {
        return userID;
    }

    public int getNtfID() {
        return ntfID;
    }

    public String getNtfMessage() {
        return ntfMessage;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "userID=" + userID +
                ", ntfID=" + ntfID +
                ", ntfMessage='" + ntfMessage + '\'' +
                '}';
    }

    public Notification setUserID(int userID) {
        this.userID = userID;
        return this;
    }

    public Notification setNtfID(int ntfID) {
        this.ntfID = ntfID;
        return this;
    }

    public Notification setNtfMessage(String ntfMessage) {
        this.ntfMessage = ntfMessage;
        return this;
    }
}