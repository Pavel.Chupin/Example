package com.example.dao;

import com.example.Data.User;
import com.example.business.UserBusinessObject;
import com.example.userDAO.DBUserDAO;

import java.util.*;
import java.sql.SQLException;

/**
 * Hello world!
 */
public class Main {
    private static final String JDBC_URL = "jdbc:h2:mem:test";
    private static final String USER_LOGIN = "sa";
    private static final String USER_PASSWORD = "";

    public static void main(String[] args) throws SQLException {
        DBUserDAO dbUserDAO = new DBUserDAO(JDBC_URL, USER_LOGIN, USER_PASSWORD);

        UserBusinessObject userBusinessObject = new UserBusinessObject(dbUserDAO);
/*
        List<User> usersInsert = new ArrayList<User>();
        usersInsert.add(new User().setUser_id(3).setLogin("pchupin").setPassword("14855887").setFirst_name("pavel").setLast_name("chupin").setGender(1).setBirth_date("1984-06-11").setDescription("test1"));
        usersInsert.add(new User().setUser_id(4).setLogin("pchupin1").setPassword("148558871").setFirst_name("pavel").setLast_name("chupin").setGender(0).setBirth_date("1984-06-11").setDescription("test1"));
        //Запускаем добавление
        dbUserDAO.insertUser(usersInsert);
        List<User> users = null;
        List<Integer> id = new ArrayList<Integer>();
        id.add(3);
        users = dbUserDAO.selectUser("","","",0);
        System.out.println(users);
        dbUserDAO.deleteUser(id);
        users = dbUserDAO.selectUser("","","",0);
        System.out.println(users);
*/
        try {
            dbUserDAO.connectionClose();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
