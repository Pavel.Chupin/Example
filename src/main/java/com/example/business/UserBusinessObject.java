package com.example.business;

import com.example.Data.Notification;
import com.example.Data.User;
import com.example.userDAO.UserDAO;
import org.apache.cayenne.CayenneRuntimeException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Summoner on 17.10.2017.
 */
public class UserBusinessObject {
    private UserDAO dbUserDAO;

    public UserBusinessObject(UserDAO dbUserDAO) {
        this.dbUserDAO = dbUserDAO;
    }

    public List<Notification> dsUserMassCreate(List<User> users) {
        List<Notification> notifications = new ArrayList<Notification>();
        List<User> usersInsert = new ArrayList<User>();

        for (User user : users) {
            if (user.getUser_id() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <User_id>"));
                continue;
            }
            if (user.getLogin() == null || user.getLogin().length() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <Login>"));
                continue;
            }
            if (user.getPassword() == null || user.getPassword().length() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <Password>"));
                continue;
            }
            if (user.getFirst_name() == null || user.getFirst_name().length() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <First_name>"));
                continue;
            }
            if (user.getLast_name() == null || user.getLast_name().length() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <Last_name>"));
                continue;
            }

            if (user.getGender() > 1) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <Gender>"));
                continue;
            }
            if (user.getBirth_date() == null || user.getBirth_date().length() == 0) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(1).setNtfMessage("Отсутствует обязательное поле <Birth_date>"));
                continue;
            }

            try {
                List<User> users_temp = dbUserDAO.selectUser("", "", "", user.getUser_id());
                if (/*users_temp != null || */!users_temp.isEmpty()) {
                    notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(2).setNtfMessage("Пользователь с таким идентификатором уже существует"));
                    continue;
                }
            } catch (CayenneRuntimeException e) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(3).setNtfMessage("Системная ошибка"));
            }

            //Добавим пользователя в список к добавлению
            usersInsert.add(user);
        }

        //Запускаем добавление пользователя в БД
        try {
            dbUserDAO.insertUser(usersInsert);
            //Если ошибок добавления нет, то запускаем добавление об успешности записи
            for (User user : usersInsert) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(0).setNtfMessage(""));
            }
        } catch (CayenneRuntimeException e) {
            //e.printStackTrace();
            //Если случилась ошибка, то все пользователи добавляются в List<Notification> с ошибкой, т.к. добавление идет в транзакции
            for (User user : usersInsert) {
                notifications.add(new Notification().setUserID(user.getUser_id()).setNtfID(3).setNtfMessage("Системная ошибка"));
            }
        }

        return notifications;
    }

    public List<Notification> dsUserMassDelete(List<Integer> usersID) {
        List<Notification> notifications = new ArrayList<Notification>();
        List<Integer> usersIDDelete = new ArrayList<Integer>();

        for (Integer userID : usersID) {
            List<User> users_temp = null;
            try {
                users_temp = dbUserDAO.selectUser("", "", "", userID);
                if (users_temp == null || users_temp.isEmpty()) {
                    notifications.add(new Notification().setUserID(userID).setNtfID(4).setNtfMessage("Пользователь с таким идентификатором не существует"));
                } else {
                    usersIDDelete.add(userID);
                }
            } catch (CayenneRuntimeException e) {
                notifications.add(new Notification().setUserID(userID).setNtfID(3).setNtfMessage("Системная ошибка"));
            }
        }

        try {
            dbUserDAO.deleteUser(usersIDDelete);
            for (Integer userID : usersIDDelete) {
                notifications.add(new Notification().setUserID(userID).setNtfID(0).setNtfMessage(""));
            }
        } catch (CayenneRuntimeException e) {
            //e.printStackTrace();
            //Если случилась ошибка, то все пользователи добавляются в List<Notification> с ошибкой, т.к. добавление идет в транзакции
            for (Integer userID : usersIDDelete) {
                notifications.add(new Notification().setUserID(userID).setNtfID(3).setNtfMessage("Системная ошибка"));
            }
        }
        return notifications;
    }

    public List<User> dsUserFindListByParam(String login, String firstName, String lastName) throws CayenneRuntimeException {
        List<User> users = new ArrayList<User>();

        try {
            users = dbUserDAO.selectUser(login, firstName, lastName, 0);
        } catch (CayenneRuntimeException e) {
            //e.printStackTrace();
            throw e;
        }
        return users;
    }

}
