package ru.proghouse.cayenne.persistent;

import ru.proghouse.cayenne.persistent.auto._UserDAO;

public class UserDAO extends _UserDAO {

    private static UserDAO instance;

    private UserDAO() {}

    public static UserDAO getInstance() {
        if(instance == null) {
            instance = new UserDAO();
        }

        return instance;
    }
}
